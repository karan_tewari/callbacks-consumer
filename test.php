<!DOCTYPE html>
<html>

<head>
    <title>Consumer Side</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <script src="../uploads/wow.min.js"></script>
    <link href="../css/animate.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2 class="pagehead">Welcome to the call backs page</h2>
        <div class="sessionActiveOrNot">
            <span>Current Status of Session : </span>
            <div class="sessionActiveGreen" id="GreenIsGo" style="visibility:hidden">
                <span class="ActiveSession">Active</span>
                <div class="GreenBlink">
                    
                </div>
            </div>
            <div class="sessionInactiveRed" id="RedIsNo" style="visibility:visible">
                <span class="InactiveSession">Inactive</span>
                <div class="RedBlink">
                    
                </div>
            </div>
        </div>
        <h3 class="pagesubhead">This page is meant to check the call backs when call being made from the consumer side</h3>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="readmepopup">
            <div class="container">
                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Readme</button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">About this module</h4>
                            </div>
                            <div class="modal-body">
                                <p>This module is meant to check and observe various callbacks being made on agent side with starting to end of the call.
                                    <br/>
                                    <br/> When a call is made to agent, staring from the very first step various call backs are made in order to get a successfull calling experience
                                    <br/>
                                    <br/> Through this module you will basically get an idea about how the stuff works, what functions are called and how everything is working. Please <b>note</b> that this is just the prototype and many features are being added to it day by day. For now you can brodaly see the major call backs happening.
                                    <br/>
                                    <br/> The current status button shows you the status of the call in simpe english language.
                                    <br/>
                                    <br/> The current method button gives you the information about which is the current callback is being called and which method is allowing you to experience the current feature being used.
                                    <br/>
                                    <br/> Press on the start button call and then you can see the being filled with the current status and callbeing returned.
                                    <br/>
                                    <br/> There is call back method sheet is also being built through which you will be able to get the detailed info on callbacks and you can also post your queries and suggestions on improving this module
                                    <br/>
                                    <br/> Hope you find it informative
                                    <br/>
                                    <br/> PS there's no terms and conditions checkbox in this module :) .
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="startCallDiv">
            <button id="go" class="startCallButton">Start Support</button>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="textShowCallbacks">
                <span class="firstText">Current Status :</span> <span class="showCallbacks"></span>
            </div>
        </div>
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="textShowCallbacks">
                <span class="firstText">Current method :</span> <span class="currentCallbackMethod"></span>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="endCallDiv">
            <button id='end' class="endCallButton">End Support</button>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="cobrowseControls">
                <button id='pauseCobrowse'>Pause Cobrowse</button>
            </div>
        </div>
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="cobrowseControls">
                <button id='resumeCobrowse'>Resume Cobrowse</button>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="annotationsDivision">
            <button id='annotationToggle'>Annotation Information</button>
            <div class="annotationsInfo" id="annodiv" style="display:none;">
                <p>
                    Information about annotations
                    <br/>
                    <span class="annotationsStroke"></span>
                    <span class="annotationsOpacity"></span>
                    <span class="annotationsWidth"></span>
                    <span class="annotationsLength"></span>
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="cobrowseOnlyDivision">
        <button id="cobrowseonly" class="btn btn-primary">Co-browse Only Mode</button>
        <h3 id="short-code"></h3>
        </div>
    </div>
</body>
<script type="text/javascript" src="https://192.168.4.12:8443/assistserver/sdk/web/consumer/assist.js"></script>
<script>
    document.getElementById('go').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent1',
            videoMode: "full"
        });
    };
    document.getElementById('end').onclick = function(event) {
        AssistSDK.endSupport();
    };
    document.getElementById('pauseCobrowse').onclick = function(event) {
        AssistSDK.pauseCobrowse();
    };
    document.getElementById('resumeCobrowse').onclick = function(event) {
        AssistSDK.resumeCobrowse();
    };
    $('#annotationToggle').click(function() {
        $('#annodiv').toggle();
    });
</script>
    <!--<script>
        if(AssistSDK.onConnectionEstablished){
            $('#GreenIsGo').css("visibility","visible");
            $('#RedIsNo').css("visibility","hidden");
        }
    </script>-->
<script>
    var requestCobrowse = function() {
        console.log("Co-browse requested");
        // window.displayAlert(translations.awaitingCobrowseResponse, "alert-info", "cobrowse-alert");
        AssistAgentSDK.requestScreenShare();
    };
</script>
<script>
    AssistSDK.onWebcamUseAccepted = function() {
        $('.showCallbacks').text('You granted cam permission');
        $('.currentCallbackMethod').text('AssistSDK.onWebcamUseAccepted');
    }
    AssistSDK.onConnectionEstablished = function() {
        $('.showCallbacks').text('call accepted by the agent');
        $('.currentCallbackMethod').text('AssistSDK.onConnectionEstablished');
        $('#GreenIsGo').css("visibility","visible");
        $('#RedIsNo').css("visibility","hidden");
    };
    AssistSDK.agentRequestedCobrowse = function() {
        $('shwoCallbacks').text('agent requested cobrose');
    }
    AssistSDK.onCobrowseActive = function() {
        $('.showCallbacks').text('cobrowse active');
        $('.currentCallbackMethod').text('AssistSDK.onCobrowseActive');
    };
    AssistSDK.onAnnotationAdded = function() {
        $('.showCallbacks').text('agent is adding annotations in page');
        $('.currentCallbackMethod').text('AssistSDK.onAnnotationAdded');
        $('.annotationsInfo').css("display", "block");
        AssistSDK.onAnnotationAdded = function(annotation, agent) {
            $('.annotationsStroke').text('Stroke type of annotation is : ' + annotation.stroke);
            $('.annotationsOpacity').text('Opacity of annotation is : ' + annotation.strokeOpacity);
            $('.annotationsWidth').text('Width of annotation is : ' + annotation.strokeWidth);
            $('.annotationsLength').text('path array length of annotation is : ' + annotation.points.length);
        };
        AssistSDK.onAnnotationsCleared = function() {
            $('.showCallbacks').text('agent has cleared the annotations in page');
        };
    };
    AssistSDK.onPushRequest = function(allow, deny) {
        var confirmation = confirm('the agent is trying to show a document, would you like to view the same?');
        if (confirmation) {
            allow();
            $('.showCallbacks').text('accpeted the document to view');
            $('.currentCallbackMethod').text('AssistSDK.onPushRequest and allow()');
            AssistSDK.onDocumentReceivedSuccess = function() {
                $('.showCallbacks').text('document recieved successfully');
                $('.currentCallbackMethod').text('AssistSDK.onDocumentReceivedSuccess');
            };
            AssistSDK.sharedDocument.onClosed = function() { // not working !!!!!
                $('.showCallbacks').text('document closed');
                $('.currentCallbackMethod').text('AssistSDK.onPushRequest and deny()');
            };
        } else {
            deny();
            $('.showCallbacks').text('rejected the document');
            $('.currentCallbackMethod').text('AssistSDK.onPushRequest and deny()');
        }
    };
    AssistSDK.onError = function(error) {
        $('.showCallbacks').text('Please check the internet connection as the session is experiecing some connectivity errors');
    }
    AssistSDK.onEndSupport = function() {
        $('.showCallbacks').text('call has ended');
        $('.currentCallbackMethod').text('AssistSDK.onEndSupport');
        $('#GreenIsGo').css("visibility","hidden");
        $('#RedIsNo').css("visibility","visible");
    };
</script>
    <script>
        var $codeDisplay = $('#short-code');
        $('#cobrowseonly').click(function (event) {
		event.preventDefault();
		
		// Retrieve a short code
		$.ajax('https://192.168.4.12:8443/assistserver/shortcode/create', {
			method: 'put',
			dataType: 'json'
		})
		.done(function (response) {

			// Retrieve session configuration (session token, CID etc.)
			$.get('https://192.168.4.12:8443/assistserver/shortcode/consumer', {
				appkey: response.shortCode
			}, function (config) {

				// translate the config retrieved from the server, into
				// parameters expected by AssistSDK
				var params = {
					cobrowseOnly: true,
					correlationId: config.cid,
					scaleFactor: config.scaleFactor,
					sessionToken: config['session-token']
				};

				// start the support session
				AssistSDK.startSupport(params);
				
				// display the code for the consumer to read to the agent
				$codeDisplay.text(response.shortCode);
			}, 'json');
		});
	});


	// configure Assist to automatically approve share requests
	AssistSDK.onScreenshareRequest = function () {
		return true;
	};

	// the $endSupport button will be hidden
	// $endButton.hide().css('display', 'inline');

	// when screenshare is active, toggle the visibility of the 'end' button

    </script>

</html>